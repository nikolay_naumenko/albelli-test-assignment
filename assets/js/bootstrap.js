/**
 * This file is part of albelli test assignment.
 */

import {Promise} from 'es6-promise';
import 'whatwg-fetch';
import App from "./app";
import PostForm from "./components/post-form";
import PostModel from "./models/post-model";
import Validator from "./utils/validator";
import Http from "./utils/http";
import Posts from "./components/posts";
import Words from "./components/words";

if (!window.Promise) {
    Promise.polyfill();
}

document.addEventListener("DOMContentLoaded", () => {
    let app = new App(),
        key = 'AIzaSyAkJQQytyUdxKzp88fwfHch8G1-NuV-ScU',
        csrf = document.querySelector('meta[name="csrf-token"]').content;

    new Posts(app);
    new Words(app);
    new PostForm(app, '.form', new PostModel(new Http(key, csrf), Validator), null);
});

