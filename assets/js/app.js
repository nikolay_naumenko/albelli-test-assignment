/**
 * This file is part of albelli test assignment.
 */

export default class App {
    constructor() {
        this.listeners = [];
    }
    registerListener(listener) {
        this.listeners.push(listener);
    }
    notify(eventData) {
        for (let listener in this.listeners) {
            this.listeners[listener](eventData);
        }
    }
}
