/**
 * This file is part of albelli test assignment.
 */

import Model from './model';

export default class PostModel extends Model {
    constructor(http, validator) {
        super(http);
        this.postUrl = '/post';
        this.getUrl = '';
        this.validator = validator;
        this.valid = false;
        this.attributes = {
            title: '',
            body: '',
            email: '',
            file: ''
        };
    }
    validate(callback) {
        this.valid = true;

        if (this.attributes['title'] === '') {
            this.valid = false;
            callback('title', 'title-empty');
        }

        if (this.attributes['email'] === '') {
            this.valid = false;
            callback('email', 'email-empty');
        }

        if (this.validator.validateEmail(this.attributes['email'])) {
            this.valid = false;
            callback('email', 'email-email');
        }

        if (this.attributes['body'] === '' && this.attributes['file'] === '') {
            callback('body', 'body-file');
        }

        return this.valid;
    }
}