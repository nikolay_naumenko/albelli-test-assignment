/**
 * This file is part of albelli test assignment.
 */

export default class Model {
    constructor(http) {
        this.http = http;
        this.postUrl = '';
        this.attributes = {};
    }
    post() {
        if (this.postUrl !== '') {
            return this.http.post(this.postUrl, this.createForm());
        }
    }
    set(name, value) {
        this.attributes[name] = value;
    }
    get(name) {
        return this.attributes[name];
    }
    getAttributes() {
        return this.attributes;
    }
    getAttributesNames() {
        return Object.keys(this.attributes);
    }
    validate() {
        return true;
    }
    createForm() {
        let form = new FormData();
        for (let element in this.attributes) {
            form.append(element, this.attributes[element]);
        }
        return form;
    }
}
