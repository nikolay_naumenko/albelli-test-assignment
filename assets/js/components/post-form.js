/**
 * This file is part of albelli test assignment.
 */

import Form from "./form";

export default class PostForm extends Form {
    getSelectorPrefix(name) {
        return this.formSelector + '__' + name + '-';
    }
}