/**
 * This file is part of albelli test assignment.
 */

import DomHelper from "../utils/dom-helper";
import Helpers from "../utils/helpers";
import Component from "./component";

export default class Form extends Component {
    constructor(app, formSelector, model, validator) {
        super(app);
        this.app = app;
        this.formSelector = formSelector;
        this.model = model;
        this.validator = validator;
        this.validationStarted = false;
        this.bindEvents();
    }
    getAttributes() {
        return this.model.getAttributes();
    }
    post() {
        this.disableForm();
        this.model.post().then(response => {
            this.enableForm();

            if (response.status !== 200) {
                throw response;
            }

            return response.json();
        }).then(json => {
            this.processResponse(json);
        })
        .catch(e => {
            this.handleServerErrors(e);
        });
    }
    handleServerErrors(response) {
        switch (response.status) {
            case 400:
            case 403: {
                DomHelper.show(this.getElement('.error-' + response.status));
                break;
            }
            default: {
                DomHelper.show(this.getElement('.error-unknown'));
            }
        }

    }
    setFormElementValue(name, value) {
        let element = this.getFormElement(name);
        if (element.type !== 'file') {
            this.getFormElement(name).value = value;
        }
    }
    disableForm() {
        for (let element of this.model.getAttributesNames()) {
            DomHelper.setDisable(this.getFormElement(element));
        }
        DomHelper.setDisable(this.getElement('.form__button-submit'));
        DomHelper.show(this.getElement('.form__spinner-wrapper'), 'flex');
    }
    enableForm() {
        for (let element of this.model.getAttributesNames()) {
            DomHelper.setDisable(this.getFormElement(element), false);
        }
        DomHelper.setDisable(this.getElement('.form__button-submit'), false);
        DomHelper.hide(this.getElement('.form__spinner-wrapper'));
    }
    processResponse(response) {
        this.app.notify(response);
        this.clearForm();
    }
    clearForm() {
        this.validationStarted = false;
        for (let key of this.model.getAttributesNames()) {
            this.set(key, '');
            let element = this.getFormElement(key);
            if (element && element.type === 'file') {
                element.value = '';
                this.updateFileInput(key, element);
            }
        }
    }
    set(name, value) {
        this.model.set(name, value);
        this.setFormElementValue(name, value);
        if (this.validationStarted) {
            this.validate();
        }
    }
    get(name) {
        return this.model.get(name);
    }
    bindEvents() {
        this.getElement().addEventListener('submit', (e) => {
            e.preventDefault();
            this.validationStarted = true;
            if (this.validate()) {
                this.post();
            } else {
                DomHelper.setDisable(this.getFormElement('submit'));
            }
        });

        for (let key of this.model.getAttributesNames()) {
            this.bindFormElement(key);
        }
    }
    bindFormElement(name) {
        let element = this.getFormElement(name);
        if (element) {
            switch (element.type) {
                case 'file': {
                    this.bindFileInputEvents(name, element);
                    break;
                }
                default: {
                    element.addEventListener('input', Helpers.throttle((e) => {
                        this.set(name, e.target.value);
                    }, 200));
                }
            }
        }
    }
    bindFileInputEvents(name, selector) {
        let button = this.getFormElement(name + '-button');
        if (button) {
            button.addEventListener('click', (e) => {
                e.preventDefault();
                selector.click();
            });
            selector.addEventListener('change', (e) => {
                e.preventDefault();
                this.updateFileInput(name, e.target);
            });
        }
    }
    updateFileInput(name, selector) {
        let selectorPrefix = this.getSelectorPrefix(name);
        let removeFileHandler = (e) => {
            e.preventDefault();
            this.getElement(selectorPrefix + 'input').value = '';
            this.updateFileInput(name, selector);
        };
        if (selector.files.length !== 0) {
            DomHelper.show(this.getElement(selectorPrefix + 'path-to'), 'inline-block');
            this.getElement(selectorPrefix + 'name').innerText = selector.files[0].name;
            this.getElement(selectorPrefix + 'remove').addEventListener('click', removeFileHandler.bind(this));
            this.set(name, selector.files[0]);
        } else {
            DomHelper.hide(this.getElement(selectorPrefix + 'path-to'));
            this.getElement(selectorPrefix + 'name').innerText = '';
            this.getElement(selectorPrefix + 'remove').removeEventListener('click', removeFileHandler);
            this.set(name, '');
        }
    }
    getSelectorPrefix(name) {
        return this.formSelector + name;
    }
    getFormElement(name) {
        return this.getElement('*[name="' + name + '"]');
    }
    getElement(name = '') {
        return DomHelper.query(this.formSelector + ' ' + name);
    }
    getElements(name = '') {
        return DomHelper.queryAll(this.formSelector + ' ' + name);
    }
    validate() {
        this.removeValidationErrors();
        let isValid= this.model.validate(this.showValidationErrors.bind(this));
        DomHelper.setDisable(this.getFormElement('submit'), !isValid);
        return isValid;
    }
    showValidationErrors(elementName, type) {
        let element = this.getFormElement(elementName);
        if (element) {
            DomHelper.addClass(element, 'error');
        }
        DomHelper.show(this.getElement('.error-' + type));
    }
    removeValidationErrors() {
        for (let attribute of this.model.getAttributesNames()) {
            DomHelper.removeClass(this.getFormElement(attribute), 'error');
        }
        for (let element of this.getElements('.form__error')) {
            DomHelper.hide(element);
        }
    }
}