/**
 * This file is part of albelli test assignment.
 */

export default class Component {
    constructor(app) {
        this.app = app;
        app.registerListener(this.appListener.bind(this))
    }
    appListener(eventData) {
        return;
    }
}