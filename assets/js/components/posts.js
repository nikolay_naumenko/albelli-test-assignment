/**
 * This file is part of albelli test assignment.
 */

import DomHelper from "../utils/dom-helper";
import Component from "./component";

export default class Posts extends Component {
    appListener(evenData) {
        if (evenData.post) {
            DomHelper.query('.posts').innerHTML = evenData.post + DomHelper.query('.posts').innerHTML;
            let noDataElement = DomHelper.query('.post__no-data');
            if (noDataElement) {
                noDataElement.innerHTML = '';
            }
        }
        return true;
    }
}