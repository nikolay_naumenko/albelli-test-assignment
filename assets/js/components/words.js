/**
 * This file is part of albelli test assignment.
 */

import Component from "./component";
import DomHelper from "../utils/dom-helper";

export default class Words extends Component {
    updateWordsList(words) {
        return DomHelper.query('.words').innerHTML = words;
    }
    appListener(evenData) {
        if (evenData.words) {
            return this.updateWordsList(evenData.words);
        }
    }
}