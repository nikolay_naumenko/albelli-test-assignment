/**
 * This file is part of albelli test assignment.
 */

export default class DomHelper {
    static addClass(element, className) {
        className = className.trim();
        let classes = ' ' + element.className + ' ';
        if (classes.indexOf(' ' + className + '') === -1) {
            element.className = (classes + className).trim();
        }
    }
    static removeClass(element, className) {
        className = className.trim();
        let classes = ' ' + element.className + ' ';
        while (classes.indexOf(' ' + className + '') !== -1) {
            classes = classes.replace(' ' + className + ' ', ' ');
        }
        if (element.className !== classes) {
            element.className = classes.trim();
        }
    }
    static query(query) {
        return document.querySelector(query);
    }
    static queryAll(query) {
        return document.querySelectorAll(query);
    }
    static show(element, display = 'block') {
        if (element) {
            element.style.display = display;
        }
    }
    static hide(element) {
        if (element) {
            element.style.display = 'none';
        }
    }
    static setDisable(element, value = true) {
        if (element) {
            element.disabled = value;
        }
    }
}