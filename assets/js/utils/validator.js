/**
 * This file is part of albelli test assignment.
 */

export default class Validator {
    static validateEmail (value) {
        return !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value);
    }
}