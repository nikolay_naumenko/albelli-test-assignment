/**
 * This file is part of albelli test assignment.
 */

export default class Helpers {
    static debounce(callback, wait, immediate = false) {
        let timeout;
        return function () {
            let callNow = immediate && !timeout;

            clearTimeout(timeout);

            timeout = setTimeout(() => {
                timeout = null;
                if (!immediate) {
                    callback.apply(this, arguments);
                }
            }, wait);

            if (callNow) callback.apply(this, arguments);
        };
    };
    static throttle(callback, wait) {
        let timeout;
        return function () {
            if (!timeout) {
                timeout = setTimeout(() => {
                    timeout = null;
                    callback.apply(this, arguments);
                }, wait);
            }
        };
    };
}