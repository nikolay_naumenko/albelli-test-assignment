/**
 * This file is part of albelli test assignment.
 */

export default class Http {
    constructor(key, csrf) {
        this.key = key;
        this.csrf = csrf;
    }
    get(url) {
        return fetch(url, 'get');
    }
    post(url, data) {
        return this.sendRequest(url, 'post', data);
    }
    put(url, data) {
        return this.sendRequest(url, 'put', data);
    }
    patch(url, data) {
        return this.sendRequest(url, 'patch', data);
    }
    delete(url) {
        return fetch(url, 'delete');
    }
    sendRequest(url, method, data = '') {
        let request = {
            method,
            credentials: 'same-origin'
        };

        if (data !== '') {
            request.body = data;
        }

        request.headers = {
            'Cache-Control': 'no-cache',
            'Authorization': 'key=' + this.key,
            'X-CSRF-TOKEN': this.csrf
        }

        return  fetch(url, request);
    }
}