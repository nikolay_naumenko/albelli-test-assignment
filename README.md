# albelli test assignment

## Installation
You must have Docker and Node.js installed on your system. Clone this repository and then run following commands within project root directory: 

Frontend:

```bash
> npm install
> npm rum prod
```

Backend:

```bash
> docker-compose build
> docker-compose up -d
> docker-compose run cli /bin/bash
# cd /app
# composer install
```

Navigate your browser to `http://localhost/`

## Configuration

All configuration options are stored in .env file (the file should not be included in repository but I added it for your convenience).

```
APP_NAME="Albelli Test"
APP_ENV=dev
APP_DEBUG=true
APP_URL=http://localhost
APP_KEY=AIzaSyAkJQQytyUdxKzp88fwfHch8G1-NuV-ScU
AUTHOR_EMAIL=test@albelli-jobs.com

IMAGE_STORE_PATH=/img/uploads
DATA_STORE_PATH=/data
```

All data stored in `/data` directory, you can safely clean it at any time. Images are stored in `/public/img/upload` directory.

    
## TO DO

- Add more abstractions
- Use events to notify services about data changes
- Add unit tests
- ...

