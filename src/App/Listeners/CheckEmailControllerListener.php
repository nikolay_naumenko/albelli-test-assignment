<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Listeners;

use App\Controllers\EmailAuthenticatedControllerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class CheckEmailControllerListener
 * @package App\Listeners
 */
class CheckEmailControllerListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var
     */
    protected $authorEmail;

    /**
     * CheckEmailControllerListener constructor.
     * @param ContainerInterface $container
     * @param $authorEmail
     */
    public function __construct(ContainerInterface $container, $authorEmail)
    {
        $this->container = $container;
        $this->authorEmail = $authorEmail;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof EmailAuthenticatedControllerInterface) {
            if ($this->authorEmail !== $event->getRequest()->get('email')) {
                throw new AccessDeniedHttpException('This action needs a valid email address');
            }
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}
