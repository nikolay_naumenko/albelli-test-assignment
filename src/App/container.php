<?php
/**
 * This file is part of albelli test assignment.
 */

use Symfony\Component\DependencyInjection;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpKernel;
use Symfony\Component\Routing;
use Symfony\Component\EventDispatcher;
use Symfony\Component\Security\Csrf;
use Symfony\Component\Validator\Validation;

$container = new DependencyInjection\ContainerBuilder();

$container->register('context', Routing\RequestContext::class);
$container->register('matcher', Routing\Matcher\UrlMatcher::class)
    ->setArguments([$routes, new Reference('context')]);

$container->register('request_stack', HttpFoundation\RequestStack::class);
$container->register('controller_resolver', \Kernel\Controller\ControllerResolver::class)
    ->setArguments([$container]);

$container->register('argument_resolver', HttpKernel\Controller\ArgumentResolver::class);

$container->register('listener.router', HttpKernel\EventListener\RouterListener::class)
    ->setArguments([new Reference('matcher'), new Reference('request_stack')]);

$container->register('listener.response', HttpKernel\EventListener\ResponseListener::class)
    ->setArguments(['UTF-8']);

$container->register('listener.kernel.controller.csrf', Kernel\Listeners\KernelControllerListener::class)
    ->setArguments([$container, getenv('APP_KEY')]);

$container->register('listener.kernel.controller.email', App\Listeners\CheckEmailControllerListener::class)
    ->setArguments([$container, getenv('AUTHOR_EMAIL')]);

$container->register('listener.exception', HttpKernel\EventListener\ExceptionListener::class)
    ->setArguments(['App\Controllers\ErrorController::exception']);

$container->register('dispatcher', EventDispatcher\EventDispatcher::class)
    ->addMethodCall('addSubscriber', [new Reference('listener.router')])
    ->addMethodCall('addSubscriber', [new Reference('listener.response')])
    ->addMethodCall('addSubscriber', [new Reference('listener.exception')])
    ->addMethodCall('addSubscriber', [new Reference('listener.kernel.controller.csrf')])
    ->addMethodCall('addSubscriber', [new Reference('listener.kernel.controller.email')])
;


$container->register('session', HttpFoundation\Session\Session::class);
$container->register('csrf.generator', Csrf\TokenGenerator\UriSafeTokenGenerator::class);

$container->register('csrf.storage', Csrf\TokenStorage\SessionTokenStorage::class)
    ->setArguments([new Reference('session')]);

$container->register('csrf.manager', Csrf\CsrfTokenManager::class)
    ->setArguments([new Reference('csrf.generator'), new Reference('csrf.storage')]);

$container->register('kernel', \Kernel\Kernel::class)
    ->setArguments([
        new Reference('dispatcher'),
        new Reference('controller_resolver'),
        new Reference('request_stack'),
        new Reference('argument_resolver'),
    ]);

$container->register('twig_loader', \Twig_Loader_Filesystem::class)
    ->setArguments([__DIR__ . '/Views/']);

$twigArguments['cache'] = false;

if (!getenv('APP_DEBUG')) {
    $twigArguments['cache'] = __DIR__ . '/../../cache/twig';
}

$container->register('twig', \Twig_Environment::class)
    ->setArguments([new Reference('twig_loader'), $twigArguments]);

$container->register('validator', Validation::class);

$container->register('image.manager', Intervention\Image\ImageManager::class);

$container->register('service.image', App\Services\ImageService::class)
    ->setArguments([__DIR__ . '/../../public', getenv('IMAGE_STORE_PATH'), new Reference('image.manager'), [
        'image/jpeg',
        'image/png'
    ]]);

$container->register('cache.simple', Symfony\Component\Cache\Simple\FilesystemCache::class)
    ->setArguments(['', 0, __DIR__ . '/../..'.getenv('DATA_STORE_PATH')]);

$container->register('storage.posts', App\Services\FilesystemStorage::class)
    ->setArguments(['storage.posts', new Reference('cache.simple')]);

$container->register('storage.words', App\Services\FilesystemStorage::class)
    ->setArguments(['storage.words', new Reference('cache.simple')]);

$container->register('service.post', App\Services\PostService::class)
    ->setArguments([
        $container,
        Validation::createValidator(),
        new Reference('service.image'),
        new Reference('storage.posts')
    ]);

$container->register('service.words', App\Services\WordsService::class)
    ->setArguments([new Reference('storage.words')]);

return $container;
