<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Services;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PostService
 * @package App\Services
 */
class PostService
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var ImageServiceInterface
     */
    protected $imageService;
    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * PostService constructor.
     * @param ContainerInterface $container
     * @param ValidatorInterface $validator
     * @param ImageServiceInterface $imageService
     * @param StorageInterface|null $storage
     */
    public function __construct(
        ContainerInterface $container,
        ValidatorInterface $validator,
        ImageServiceInterface $imageService,
        StorageInterface $storage = null
    ) {
        $this->validator = $validator;
        $this->container = $container;
        $this->imageService = $imageService;
        $this->storage = $storage;
    }

    /**
     * @param string $title
     * @param string $email
     * @param string $body
     * @param UploadedFile|null $image
     * @return array|bool
     */
    public function validateAndCreate(string $title, string $email, string $body = '', UploadedFile $image = null)
    {
        if (!$this->validate($title, $email, $body, $image)) {
            return false;
        }

        return $this->createPost($title, $email, $body, $image);
    }

    /**
     * @param string $title
     * @param string $email
     * @param string $body
     * @param UploadedFile|null $image
     * @return bool
     */
    public function validate(string $title, string $email, string $body = '', UploadedFile $image = null)
    {
        $titleViolations = $this->validator->validate($title, [
            new NotBlank()
        ]);

        $emailViolations = $this->validator->validate($email, [
            new NotBlank(),
            new Email()
        ]);

        $bodyViolations = $this->validator->validate($body, [
            new NotBlank(),
        ]);

        $validImage = false;

        if ($image) {
            $validImage = $this->imageService->validate($image);
        }

        return (
            count($titleViolations) === 0
            && count($emailViolations) === 0
            && (count($bodyViolations) === 0 || $validImage)
        );
    }

    /**
     * @param string $title
     * @param string $email
     * @param string $body
     * @param UploadedFile|null $image
     * @return array
     */
    public function createPost(string $title, string $email, string $body = '', UploadedFile $image = null)
    {
        $images = [];

        if ($image) {
            $images = $this->imageService->save($image, [
                'image' => [300, 300],
                'image2x' => [600, 600],
            ]);
        }

        $newPost = [
            'title' => $title,
            'email' => $email,
            'body' => $body,
            'image' => $images['image'],
            'image2x' => $images['image2x'],
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $this->storage->create($newPost);

        return $newPost;
    }

    /**
     * @return array|mixed
     */
    public function getPosts()
    {
        $posts = $this->storage->getAllRecords();

        if (!is_array($posts)) {
            $posts = [];
        }

        return $posts;
    }
}
