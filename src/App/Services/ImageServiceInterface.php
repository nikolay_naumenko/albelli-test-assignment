<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface ImageServiceInterface
 * @package App\Services
 */
interface ImageServiceInterface
{
    /**
     * @param UploadedFile $file
     * @return bool
     */
    public function validate(UploadedFile $file): bool;

    /**
     * @param UploadedFile $file
     * @param array $sizes
     * @return array
     */
    public function save(UploadedFile $file, array $sizes): array;
}
