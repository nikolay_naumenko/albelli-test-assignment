<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Services;

/**
 * Interface StorageInterface
 * @package App\Services
 */
interface StorageInterface
{
    /**
     * @param array $record
     * @return mixed
     */
    public function create(array $record);

    /**
     * @return mixed
     */
    public function getAllRecords();

    /**
     * @param array $data
     * @return mixed
     */
    public function updateAllRecords(array $data);
}
