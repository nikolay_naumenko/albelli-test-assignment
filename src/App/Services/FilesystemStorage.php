<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Services;

use Psr\SimpleCache\CacheInterface;

/**
 * Class FilesystemStorage
 * @package App\Services
 */
class FilesystemStorage implements StorageInterface
{
    /**
     * @var string
     */
    protected $storageName;
    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * FilesystemStorage constructor.
     * @param string $storageName
     * @param CacheInterface $cache
     */
    public function __construct(string $storageName, CacheInterface $cache)
    {
        $this->storageName = $storageName;
        $this->cache = $cache;
    }

    /**
     * @param array $record
     */
    public function create(array $record)
    {
        $data = $this->cache->get($this->storageName, []);
        array_unshift($data, $record);
        $this->cache->set($this->storageName, $data);
    }

    /**
     * @return mixed
     */
    public function getAllRecords()
    {
        return $this->cache->get($this->storageName, []);
    }

    /**
     * @param array $data
     */
    public function updateAllRecords(array $data)
    {
        $this->cache->set($this->storageName, $data);
    }
}
