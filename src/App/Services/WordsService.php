<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Services;

/**
 * Class WordsService
 * @package App\Services
 */
class WordsService
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * WordsService constructor.
     * @param $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }


    /**
     * @param array ...$fields
     */
    public function updateWords(...$fields): void
    {
        $data = $this->storage->getAllRecords();

        foreach ($fields as $field) {
            $words = explode(' ', strip_tags($field));
            $words = array_map('trim', $words);
            $words = array_filter($words, function ($word) {
                return strlen($word) > 4;
            });

            foreach ($words as $word) {
                $data[$word] = array_key_exists($word, $data) ? $data[$word] + 1 : 1;
            }
        }
        arsort($data);
        $this->storage->updateAllRecords($data);
    }

    /**
     * @return array
     */
    public function getWords(): array
    {
        return array_slice($this->storage->getAllRecords(), 0, 5);
    }
}
