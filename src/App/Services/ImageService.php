<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Services;

use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ImageService
 * @package App\Services
 */
class ImageService implements ImageServiceInterface
{
    /**
     * @var string
     */
    protected $path;
    /**
     * @var string
     */
    protected $url;
    /**
     * @var ImageManager
     */
    protected $imageManager;
    /**
     * @var array
     */
    protected $mimeTypes;

    /**
     * ImageService constructor.
     * @param string $path
     * @param string $url
     * @param ImageManager $imageManager
     * @param array $mimeTypes
     */
    public function __construct(string $path, string $url, ImageManager $imageManager, array $mimeTypes = [])
    {
        $this->path = $path;
        $this->url = $url;
        $this->imageManager = $imageManager;
        $this->mimeTypes = $mimeTypes;
    }

    /**
     * @param UploadedFile|null $file
     * @return bool
     */
    public function validate(UploadedFile $file = null): bool
    {
        if ($file) {
            return in_array($file->getMimeType(), $this->mimeTypes);
        }

        return false;
    }

    /**
     * @param UploadedFile $file
     * @param array $sizes
     * @return array
     */
    public function save(UploadedFile $file, array $sizes): array
    {
        $uniq = uniqid();

        if (!is_dir($this->path.$this->url)) {
            mkdir($this->path.$this->url, 0755, true);
        }

        foreach ($sizes as $key => $size) {
            $url = $this->url.'/'.$key.'-'.$uniq.'.'.$file->guessExtension();
            $this->imageManager->make($file->getPathname())->fit($size[0], $size[1])->save($this->path.$url);
            $sizes[$key] = $url;
        }

        return $sizes;
    }
}
