<?php
/**
 * This file is part of albelli test assignment.
 */

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();

$routes->add('index', new Routing\Route('/', [
    '_controller' => 'App\Controllers\IndexController::index'
], [], [], '', [], [
    'GET'
]));

$routes->add('post', new Routing\Route('/post', [
    '_controller' => 'App\Controllers\PostController::post'
], [], [], '', [], [
    'POST'
]));

return $routes;
