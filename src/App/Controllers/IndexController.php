<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Controllers;

use Kernel\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class IndexController
 * @package App\Controllers
 */
class IndexController extends BaseController
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        $csrf = $this->container->get('csrf.manager')->getToken('post');
        $posts = $this->container->get('service.post')->getPosts();
        $words = $this->container->get('service.words')->getWords();

        return $this->render('index.html.twig', compact('posts', 'words', 'csrf'));
    }
}
