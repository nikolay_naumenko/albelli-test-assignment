<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Controllers;

use Kernel\Controller\BaseController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ErrorController
 * @package App\Controllers
 */
class ErrorController extends BaseController
{
    /**
     * @param FlattenException $exception
     * @return Response
     */
    public function exception(FlattenException $exception): Response
    {
        return new Response(
            $this->twig('error.html.twig', ['error' => $exception->getMessage()]),
            $exception->getStatusCode()
        );
    }
}
