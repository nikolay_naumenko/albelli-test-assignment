<?php
/**
 * This file is part of albelli test assignment.
 */

namespace App\Controllers;

use Kernel\Controller\AppKeyAuthenticatedController;
use Kernel\Controller\BaseController;
use Kernel\Controller\CsrfTokenAuthenticatedController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class PostController
 * @package App\Controllers
 */
class PostController extends BaseController implements
    CsrfTokenAuthenticatedController,
    EmailAuthenticatedControllerInterface,
    AppKeyAuthenticatedController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function post(Request $request): Response
    {
        // Words service should be run using events (like event.post_was_added), but I don't have time
        // for implementing it

        $postService = $this->container->get('service.post');
        $wordsService = $this->container->get('service.words');

        $newPost = $postService->validateAndCreate(
            $request->get('title'),
            $request->get('email'),
            $request->get('body'),
            $request->files->get('file')
        );

        if (!is_array($newPost)) {
            throw new BadRequestHttpException('Server validation error.');
        }

        $wordsService->updateWords($newPost['title'], $newPost['body']);

        $response = [
            'post' => $this->twig('posts/item.html.twig', ['post' => $newPost]),
            'words' => $this->twig('words/list.html.twig', ['words' => $wordsService->getWords()]),
        ];

        return new Response(json_encode($response), 200, [
            'Content-Type' => 'application/json'
        ]);
    }
}
