<?php
/**
 * This file is part of albelli test assignment.
 */

namespace Kernel;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

/**
 * Class Kernel
 * @package Kernel
 */
class Kernel extends HttpKernel\HttpKernel
{
    /**
     * Kernel constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param ControllerResolverInterface $resolver
     * @param RequestStack|null $requestStack
     * @param ArgumentResolverInterface|null $argumentResolver
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ControllerResolverInterface $resolver,
        RequestStack $requestStack = null,
        ArgumentResolverInterface $argumentResolver = null
    ) {
        parent::__construct($dispatcher, $resolver, $requestStack, $argumentResolver);
    }
}
