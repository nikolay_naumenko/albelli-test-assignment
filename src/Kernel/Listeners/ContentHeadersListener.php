<?php
/**
 * This file is part of albelli test assignment.
 */

namespace Kernel\Listeners;

use AFC\Events\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ContentHeadersListener
 * @package Kernel\Listeners
 */
class ContentHeadersListener implements EventSubscriberInterface
{
    /**
     * @param ResponseEvent $event
     */
    public function onResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();
        $headers = $response->headers;

        if (!$headers->has('Content-Length') && !$headers->has('Transfer-Encoding')) {
            $headers->set('Content-Length', strlen($response->getContent()));
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return ['response' => ['onResponse', -255]];
    }
}
