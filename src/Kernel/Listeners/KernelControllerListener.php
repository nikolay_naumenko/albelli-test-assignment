<?php
/**
 * This file is part of albelli test assignment.
 */

namespace Kernel\Listeners;

use Kernel\Controller\AppKeyAuthenticatedController;
use Kernel\Controller\CsrfTokenAuthenticatedController;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Csrf\CsrfToken;

/**
 * Class KernelControllerListener
 * @package Kernel\Listeners
 */
class KernelControllerListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var string
     */
    protected $appKey;

    /**
     * KernelControllerListener constructor.
     * @param ContainerInterface $container
     * @param string $appKey
     */
    public function __construct(ContainerInterface $container, string $appKey)
    {
        $this->container = $container;
        $this->appKey = $appKey;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof CsrfTokenAuthenticatedController) {
            $csrf = $event->getRequest()->headers->get('X-CSRF-TOKEN');

            if (!$this->container->get('csrf.manager')->isTokenValid(new CsrfToken('post', $csrf))) {
                throw new AccessDeniedHttpException('This action needs a valid CSRF token.');
            }
        }

        if ($controller[0] instanceof AppKeyAuthenticatedController) {
            $key = $event->getRequest()->headers->get('Authorization');

            if ('key=' . $this->appKey !== $key) {
                throw new AccessDeniedHttpException('This action needs a valid application key.');
            }
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}
