<?php
/**
 * This file is part of albelli test assignment.
 */

namespace Kernel\Controller;

/**
 * Interface CsrfTokenAuthenticatedController
 * @package Kernel\Controller
 */
interface CsrfTokenAuthenticatedController
{
    // ...
}
