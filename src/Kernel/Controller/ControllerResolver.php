<?php
/**
 * This file is part of albelli test assignment.
 */

namespace Kernel\Controller;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;

/**
 * Class ControllerResolver
 * @package Kernel\Controller
 */
class ControllerResolver extends ContainerControllerResolver
{
    /**
     * @inheritdoc
     */
    protected function createController($controller)
    {
        $resolvedController = parent::createController($controller);

        if (is_array($resolvedController) && $resolvedController[0] instanceof ContainerAwareInterface) {
            $resolvedController[0]->setContainer($this->container);
        }

        return $resolvedController;
    }
}
