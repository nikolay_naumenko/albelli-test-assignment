<?php
/**
 * This file is part of albelli test assignment.
 */

namespace Kernel\Controller;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseController
 * @package Kernel
 */
abstract class BaseController implements ContainerAwareInterface
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param string $view
     * @param array $parameters
     * @return string
     */
    public function twig(string $view, array $parameters = [])
    {
        if ($this->container->has('twig')) {
            return $this->container->get('twig')->render($view, $parameters);
        }

        return '';
    }

    /**
     * @param string $view
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    public function render(string $view, array $parameters = [], Response $response = null): Response
    {
        if ($response === null) {
            $response = new Response();
        }

        if ($this->container->has('twig')) {
            $response->setContent($this->container->get('twig')->render($view, $parameters));
        }

        return $response;
    }
}
