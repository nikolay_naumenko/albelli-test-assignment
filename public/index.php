<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();

$env = include __DIR__.'/../src/App/environment.php';
$routes = include __DIR__.'/../src/App/routes.php';
$container = include __DIR__.'/../src/App/container.php';

$kernel = $container->get('kernel');
$kernel->handle($request)->send();
